<?php
/**
 * Werkstatt child theme functions and definitions
 */

/*-----------------------------------------------------------------------------------*/
/* Include the parent theme style.css
/*-----------------------------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

add_action( 'wp_enqueue_scripts', 'werkstatt_child_script',11 );
function werkstatt_child_script() {
	// Deregister standard werkstatt-script
	wp_deregister_script('werkstatt-script');

	// Loads Custom Werkstatt Child JavaScript functionality
	wp_enqueue_script( 'werkstatt-child-script', get_stylesheet_directory_uri() . '/js/functions.js', array( 'jquery' ), '20201115', true );
	
	//wp_dequeue_style( 'genericons' );
	// Add new Genericons file to load custom Social icons
	wp_enqueue_style( 'genericons-merelellen', get_stylesheet_directory_uri() . '/genericons/genericons/genericons.css', array(), '3.3.1' );

}

add_action( 'cmb2_admin_init', 'merelellen_register_details_metabox' );

function merelellen_register_details_metabox() {
	$prefix = 'merelellen_details_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$work_details = new_cmb2_box( array(
		'id'            => $prefix . 'box',
		'title'         => esc_html__( 'Details', 'cmb2' ),
		'object_types'  => array( 'post' ), // Post type
		// 'show_on_cb' => 'merelellen_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'merelellen_add_some_classes', // Add classes through a callback.
	) );

	$work_details->add_field( array(
		'name'       => esc_html__( 'Materiaal', 'cmb2' ),
		'desc'       => esc_html__( "Bijvoorbeeld: 'acryl op doek'", 'cmb2' ),
		'id'         => $prefix . 'materials',
		'type'       => 'text',
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
		// 'column'          => true, // Display field value in the admin post-listing columns
	) );

	$work_details->add_field( array(
		'name' => esc_html__( 'Afmetingen (hoogte x breedte)', 'cmb2' ),
		'desc' => esc_html__( "Bijvoorbeeld: '40x40cm'", 'cmb2' ),
		'id'   => $prefix . 'size',
		'type' => 'text',
		// 'repeatable' => true,
		// 'column' => array(
		// 	'name'     => esc_html__( 'Column Title', 'cmb2' ), // Set the admin column title
		// 	'position' => 2, // Set as the second column.
		// );
		// 'display_cb' => 'merelellen_display_text_small_column', // Output the display of the column values through a callback.
	) );
}

/* Remove the cat / taxonomy: string from archive titles */

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});