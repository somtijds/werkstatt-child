<?php
/**
 * The template for displaying details in the single.php template
 *
 * @package Werkstatt
 * @since Werkstatt 1.0
 * @version 1.0
 */
?>

<?php $materials = get_post_meta( get_the_ID(), 'merelellen_details_materials', true ); ?>
<?php $size = get_post_meta( get_the_ID(), 'merelellen_details_size', true ); ?>

<?php if ( ! empty( $materials ) || ! empty( $size ) ) : ?>
	<div class="entry-details">
	<?php if ( ! empty( $materials ) ) : ?>
		<div class="materials">	
			<?php echo esc_html($materials); ?>
		</div>
	<?php endif; ?>
	<?php if ( ! empty( $size ) ) : ?>
		<div class="size">	
			<?php echo esc_html($size); ?>
		</div>
	<?php endif; ?>
	</div>
<?php endif; ?>