<?php
/**
 * The default template for displaying content
 *
 * @package Werkstatt
 * @since Werkstatt 1.0
 * @version 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>
	<?php if ( '' != get_the_post_thumbnail() && ! post_password_required() ) : ?>
		<div class="entry-thumbnail">
			<?php if (is_home()) : ?>
			<div class="entry-info"><span>i</span></div>
			<?php endif; ?>
			<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail('large'); ?>
			</a>
		</div><!-- end .entry-thumbnail -->
	<?php else : ?>
		<div class="entry-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" class="placeholder-img wp-post-img" />
			</a>
		</div><!-- end .entry-thumbnail -->
	<?php endif; ?>

	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<div class="entry-meta">
			<?php get_template_part( 'entry-details' ); ?>
			<?php edit_post_link( esc_html__( 'Edit', 'werkstatt' ), '<div class="entry-edit">', '</div>' ); ?>
		</div><!-- end .entry-meta -->
	</header><!-- end .entry-header -->

</article><!-- end post -<?php the_ID(); ?> -->
