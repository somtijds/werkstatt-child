<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package Werkstatt
 * @since Werkstatt 1.0
 * @version 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php get_template_part( 'entry-details' ); ?>
			<?php edit_post_link( esc_html__( 'Edit', 'werkstatt' ), '<div class="entry-edit">','</div>' ); ?>
		</div><!-- end .entry-meta -->
	</header><!-- end .entry-header -->

	<div class="content-wrap">
		<?php if ( '' != get_the_post_thumbnail() && ! post_password_required() ) : ?>
			<div class="entry-thumbnail">
				<?php if (shortcode_exists('zoom')) : ?>
				<?php echo do_shortcode('[zoom size=medium_large zoomin=3]'); ?>
				<?php else: 
					the_post_thumbnail('large'); 
				endif; ?>
			</div><!-- end .entry-thumbnail -->
		<?php endif; ?>

		<div class="entry-content">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'werkstatt' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- end .entry-content -->
	</div><!-- end .content-wrap -->
</article><!-- end .post-<?php the_ID(); ?> -->